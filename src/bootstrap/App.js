import React, {Component} from 'react';
import {RootStack} from './screens';

export default class App extends Component {
  render() {
    return <RootStack />;
  }
}
