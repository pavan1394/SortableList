import { createStackNavigator } from 'react-navigation';
import VideoTrim from '../modules/components/VideoTrim';
import HomePage from '../modules/components/HomePage';

export const RootStack = createStackNavigator(
  {
    Home: HomePage,
    VideoTrim: VideoTrim,
  },
  {
    initialRouteName: 'Home',
  }
);