import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  NativeModules
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
// import ImagePicker from 'react-native-customized-image-picker';
import { VideoPlayer, Trimmer, ProcessingManager } from 'react-native-video-processing';
import If from '../../helpers/If';
const { RNTrimmerManager: TrimmerManager } = NativeModules;

export default class VideoTrim extends Component {

	static navigationOptions = {
    title: 'Video Trim',
  };

	state = {
    videoSource: null,//'/storage/emulated/0/WhatsApp/Media/WhatsApp Video/MP4_20180822_022924.mp4',
  };

  componentWillUnmount() {
  	console.log('received unmount');
  }

  selectVideoTapped = () => {
  	const options = {
  title: 'Select Video',
  takePhotoButtonTitle: '',
  chooseFromLibraryButtonTitle: 'Choose from Gallery',
  mediaType: 'video',
  storageOptions: {
    skipBackup: true,
    path: 'Videos',
  },
};
    ImagePicker.showImagePicker(options, (response) => {

      if (response.didCancel) {
        console.log('User cancelled video picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else {
      	console.log('path', response.path);
      	// this.setPath(response.path);
       const opts = {
            startTime: 0,
            endTime: 15,
            mediaType: 'video',
            videoQuality: 'medium',
            storageOptions: {
              skipBackup: true,
              path: 'videos',
           }
        };
        ProcessingManager.trim(response.path, opts) 
          .then((data) => {
            if (data) {
              alert('Trimed video is saved at this path' + data)
            } else {
              alert('Video trim is failed');
            }
          });
      }
    });
  }

  setPath = (source) => {
    if (source) {
    this.setState({videoSource: source});
    }
    console.log('video source', this.state.videoSource);
  };

  trimVideo = (e) => {
  	console.log('Trimmer time', e);
        const options = {
            startTime: 0,//e.startTime,
            endTime: 15,//e.endTime,
            storageOptions: {
      		skipBackup: true,
      		path: 'videos',
      		},
        };
         this.videoPlayerRef.trim(options)
            .then((newSource) => console.log('newSource',newSource))
            .catch(console.warn);
    }

  render() {
    return (
      <View style={{flex: 1,}}>
      <If condition={this.state.videoSource}>
      <VideoPlayer
                    ref={ref => this.videoPlayerRef = ref}
                    startTime={30}  
                    endTime={120}   
                    play={true}     
                    replay={false}   
                    rotate={true}   
                    source={this.state.videoSource}
                    style={{ backgroundColor: 'black' }}
                    resizeMode={VideoPlayer.Constants.resizeMode.CONTAIN}
                    onChange={({ nativeEvent }) => console.log(nativeEvent)} 
                />
                <Trimmer
                    source={this.state.videoSource}
                    height={300}
                    width={300}
                    onChange={({e}) => {
                    	console.log('Trimmer', e);
                    	this.trimVideo(e);
                    }}
                />
      </If>
         <View style={{flex:1, flexDirection: 'row', padding: 10, alignItems: 'center', justifyContent: 'center'}}>
         <Button
              onPress={() => {
              	this.selectVideoTapped();
              }}
              color="#841584"
              title="Select Video"
          />
          {/*<View style={{flex:1}}/>
          <Button
              onPress={() => {this.trimVideo();
              }}
              color="#841584"
              title="Trim Video"
          />*/}
         </View>
         </View>
    );
  }
}
