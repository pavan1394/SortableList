import React, {Component} from 'react';
import {
  Animated,
  Easing,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Button
} from 'react-native';

export default class Box extends Component {

  constructor(props) {
    super(props);
    this._active = new Animated.Value(0);

    this._style = {
      transform: [{
        scale: this._active.interpolate({
          inputRange: [0, 1],
          outputRange: [1, 1],
        }),
      }],
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.active !== nextProps.active) {
      Animated.timing(this._active, {
        duration: 300,
        easing: Easing.bounce,
        toValue: Number(nextProps.active),
      }).start();
    }
  }

  render() {
    const {data} = this.props;

    return (
        <View>
          <Animated.View style={[
            styles.row,
            this._style,
          ]}>
          </Animated.View>
          <Text style={{alignSelf: 'center', padding: 10}}>{data.title}</Text>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'column',
    alignItems: 'center',
    borderColor: '#057',
    borderWidth: 1,
    padding: 10,
    width: 100,
    height: 100,
    marginHorizontal: 10,
    borderRadius: 4,
  },
});
